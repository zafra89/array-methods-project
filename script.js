var yourArr = [' hello', ' goobye', ' see you', ' welcome'];

document.getElementById('push-btn').addEventListener('click', pushMethod);
document.getElementById('pop-btn').addEventListener('click', popMethod);
document.getElementById('shift-btn').addEventListener('click', shiftMethod);
document.getElementById('unshift-btn').addEventListener('click', unshiftMethod);
document.getElementById('splice-btn').addEventListener('click', spliceMethod);
document.getElementById('slice-btn').addEventListener('click', sliceMethod);

function pushMethod() {
  event.preventDefault();
  var inputPush = document.getElementById('input-push');
  var paragraphPush = document.getElementById('push-method-paragraph');
  if (inputPush.value == '') {
    alert('Please fill the input');
  } else {
    yourArr.push(' ' + inputPush.value);
    paragraphPush.innerHTML = '[' + yourArr + ']';
  }
}

function popMethod() {
  event.preventDefault();
  var paragraphPop = document.getElementById('pop-method-paragraph');
  yourArr.pop();
  paragraphPop.innerHTML = yourArr;
}

function shiftMethod() {
  event.preventDefault();
  var paragraphShift = document.getElementById('shift-method-paragraph');
  yourArr.shift();
  paragraphShift.innerHTML = yourArr;
}

function unshiftMethod() {
  event.preventDefault();
  var inputUnshift = document.getElementById('input-unshift');
  var paragraphUnshift = document.getElementById('unshift-method-paragraph');
  if (inputUnshift.value == '') {
    alert('Please fill the input');
  } else {
    yourArr.unshift(inputUnshift.value);
    paragraphUnshift.innerHTML = '[' + yourArr + ']';
  }
}

function spliceMethod() {
  event.preventDefault();
  var startingInput = document.getElementById('input-splice-start');
  var hmeInput = document.getElementById('input-splice-hme');
  var elementInput = document.getElementById('input-splice-element');
  var paragraphSplice = document.getElementById('splice-method-paragraph');
  yourArr.splice(startingInput.value, hmeInput.value, elementInput.value);
  paragraphSplice.innerHTML = yourArr;
}

function sliceMethod() {
  event.preventDefault();
  var paragraphSlice = document.getElementById('slice-method-paragraph');
  var startingInput = document.getElementById('input-slice-start');
  var endingInput = document.getElementById('input-slice-end');
  var newArr = yourArr.slice(startingInput.value, endingInput.value);
  paragraphSlice.innerHTML = newArr;
}